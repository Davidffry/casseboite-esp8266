#include <ESP8266WiFi.h>
#include <espnow.h>
#include "Button2.h"


#define BUTTON_PIN 5
#define ESPID 3
#define POINTS 3

Button2 button;

uint8_t broadcastAddress[] = {0xBC, 0xDD, 0xC2, 0xED, 0x24, 0x0E};

// Structure example to send data
// Must match the receiver structure
typedef struct struct_message {
    int id;
    int points;
} struct_message;

// Create a struct_message called test to store variables to be sent
struct_message myData;

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("\r\nLast Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}
 
void setup() {
  // Init Serial Monitor
  Serial.begin(115200);
 
  // Set device as a Wi-Fi Station
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();

  // Init ESP-NOW
  if (esp_now_init() != 0) {
    Serial.println("Error initializing ESP-NOW");
    return;
  } 
  // Set ESP-NOW role
  esp_now_set_self_role(ESP_NOW_ROLE_CONTROLLER);

  // Once ESPNow is successfully init, we will register for Send CB to
  // get the status of Trasnmitted packet
  esp_now_register_send_cb(OnDataSent);
  
  // Register peer
  esp_now_add_peer(broadcastAddress, ESP_NOW_ROLE_SLAVE, 1, NULL, 0);

  button.begin(BUTTON_PIN);
  button.setTapHandler(tap);
}
 
void loop() {
  button.loop();
}

void tap(Button2& btn) {
    myData.id = ESPID;
    myData.points = POINTS;
    
    esp_now_send(0, (uint8_t *) &myData, sizeof(myData));
}