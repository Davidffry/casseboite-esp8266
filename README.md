# Casse-Boite ESP8266

Le but de ce repository est de créer un décompte automatique des boites tombées lors de la kermesse de mes enfants.

## Les Objectifs

les objectifs affichés pour gérer le décompte des boites tombées

- Apprendre l'électronique
    - Les ESP32/8266
    - La gestion des intercommunication entre les objets
    - La gestion de la batterie
    - Générer un Webserver 
    - Afficher le resultat soit via une interface web ou une interface "lcd"
- Faire des impressions 3d
    - Réalisation des impressions 3D pour mettre les ESP dans les boites
    - Gérer les impressions ***as code*** via openscad
- Faire communiquer des objets entres eux
    - via ESPNOW

## Materiel choisi

- ESP8266 x 1 - comme server d'API
- ESP-12E x 10 
- Jumper-Wire 
- Pile CR2040 x 10
- Boutons x 10
- Case en PLA x 11

## Pour commencer 

2 choix s'offre à moi : 
- soit faire de l'embarqué
- soit faire du OCR (optical Character Recognition)

Pour le moment j'ai décidé de prendre la première solution.
Pourquoi? Parce que j'ai envi.


Concernant l'OCR :

Pour l'OCR et suite à des recherches, je pourrais utiliser **[Tesseract JS](https://tesseract.projectnaptha.com/)** avec ESP32-CAM mais je ne l'ai pas encore tester. 

Je pourrais, également, utiliser un Raspberry pi avec une camera intégré, je ne l'ai pas choisi, il n'y a pas de raison particulière, je souhaite utiliser ESP8266 pour le moment.

## Schéma d'implementation

![Une image de casse boite](casseboite.drawio.svg)

### Les boites 

Chaque boite aura un ESP à l'interieur identifié **ET** avec une valeur définie.
Ces valeurs seront de 1 à 10 pour un cumul total de **55 points**.
Exemple:
- la boite no10 aura 10 points associé
- la boite no5 aura 5 points associé
etc.

A l'intérieur de chaque boite l'ESP sera protégé d'une coque en Impression 3D.
Vous pourrez retrouver le code de la boite dans les différents dossier de ce repo.
A la boite, sera intégré un bouton. Ce Bouton permettra d'envoyer à l'ESP Central la valeur de la boite tombée correspondante.

### L ESP central

Il y aura un ESP "CENTRAL" qui permettra de faire l'affichage des points et la remise à zero de ceux ci.

### Les ESPs intégrés aux boites

Ces ESP auront pour but de transmettre la valeur de la boite tombée à l'ESP CENTRAL.
Aujourd'hui je m'aide d'un bouton pour comptabiliser les points. **Mais** pourquoi pas s'aider d'un gyroscope.

## receiver/receiver.ino

Le fichier *receiver/receiver.ino* sera uploadé vers l'ESP CENTRAL pour récupérer l'ensemble des valeurs des boites tombées, le transfert se fera par appuyage de boutton (pour le moment).


Ici ce n'est pas la version finale.

## senders/senders.ino

Le fichier *senders/senders.ino* est uploadé vers les différents ESP dans les boites, à l'appui d'un bouton la valeur sera transmise l'ESP CENTRAL.


En cours de developpement.

# Elements à réaliser :
[ ] Faire la liste des éléments à réaliser  

## Conclusion

On aurait pu faire un tableur excel mais c'est trop facile
